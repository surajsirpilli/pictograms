void showHelp()
{
    fill(dred); 
     pushMatrix(); translate(20,20);
     text("Animating Motion using keyframes",0,0); translate(0,20);
     text("Suraj Sirpilli",0,0); translate(0,20);
     fill(dgreen); 
     translate(0,20);
     text("First click in this window to activate it",0,0); translate(0,20);
     text("Press SPACE to show/hide this help text",0,0); translate(0,20);
     text("Click on the window to drag nearest points",0,0); translate(0,20);
     text("Use the mouse wheel to twist the nearest point",0,0); translate(0,20);
     text("Use 'x' to remove points",0,0); translate(0,20);
     text("Press 'a' and mouse button to add points",0,0); translate(0,20);
     text("Press '1'/'2'/'3'/'4' to load frames 1/2/3/4 respectively",0,0); translate(0,20);
     text("Use Shift+'1'/ Shift+'2'/ Shift+'3'/ Shift+'4' to save frames 1/2/3/4 respectively",0,0); translate(0,20);
     text("Press 'A' to start/stop animation",0,0); translate(0,20);
     text("Use 'i' to use image mode",0,0); translate(0,20);
     text("Use '[' to add images to edges and ']' to remove images",0,0); translate(0,20);
   /*  text("  l: linear interpolation of corresponding endpoints",0,0); translate(0,20);
     text("  v: vertex-interplating cubic paths for endpoints",0,0); translate(0,20);
     text("  m: midpoint-interplating cubic path (linear scale, linear twist)",0,0); translate(0,20);
     text("  s: steady spirals for consecutive edge-pairs (logarithmic scale, linear twist)",0,0); translate(0,20);
     text("  e: equi-tempered spirals for consecutive edge-pairs (linear scale, linear twist)",0,0); translate(0,20);
     text("  b: Bezier of steady spirals (not interpolating)",0,0); translate(0,20);
     text("  i: interpolation attempt by retrofitting the Bezier control spines",0,0); translate(0,20);
     text("  c: Catmul-Rom of spirals",0,0); translate(0,20);
     text("  f: Four-point subdivision of spirals",0,0); translate(0,20);
     text("  p: Parabolic blend subdivision of logarithmic spirals",0,0); translate(0,20);
     text("  h/H: shortcuts to hide/show all spines",0,0); translate(0,20); */
     translate(0,20);
     text("Press 'U' to load from saved file",0,0); translate(0,20);
     text("Press 'S' to all the frames to a file",0,0); translate(0,20);
     popMatrix(); noFill();
}
