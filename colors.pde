//Colors
color col(int a, int r, int g, int b) {return a<<24 | r<<16 | g<<8 | b ;} // makes color fast with alpha (opacity), red, green, blue between 0 and 255
color col(int r, int g, int b) {return r<<16 | g<<8 | b ;} // makes color fast without alpha
color setA(color c, int a) {return (a<<24) | ((c<<8)>>8) ;} // sets alpha for color c
int colA(color c) {return (c>>24) & 0xFF;}
int colR(color c) {return (c>>16) & 0xFF;}
int colG(color c) {return (c>>8) & 0xFF;}
int colB(color c) {return c & 0xFF;}
color brown=#8B5701, red=#FF0000, magenta=#FF00FB, blue=#0300FF, cyan=#00FDFF, green=#00FF01, yellow=#FEFF00, skin=#EAAD95, black=#000000, grey=#868686, white=#FFFFFF, orange=#FFC000;
color lbrown=#896E44, lred=#FF7E7E, lmagenta=#FF7EFD, lblue=#807EFF, lcyan=#7EFFFE, lgreen=#80FF7E, lyellow=#FFFF7E, lskin=#E89273, lorange=#FFDF7E;
color dbrown=#674001, dred=#810000, dmagenta=#81007F, dblue=#000E81, dcyan=#008180, dgreen=#018100, dyellow=#808100, dskin=#815531, dorange=#816100;
color interCol(color C0, float t, color C1) {return col( int((1.-t)*colA(C0)+t*colA(C1)) , int((1.-t)*colR(C0)+t*colR(C1)) , int((1.-t)*colG(C0)+t*colG(C1)) , int((1.-t)*colB(C0)+t*colB(C1)) );}
color interColHBS(color C0, float t, color C1) {
    return color( int((1.-t)*hue(C0)+t*hue(C1)) ,  int((1.-t)*saturation(C0)+t*saturation(C1)) , 
                  int((1.-t)*brightness(C0)+t*brightness(C1)) , int((1.-t)*alpha(C0)+t*alpha(C1)) );}


