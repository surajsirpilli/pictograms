import javax.swing.*;
import java.util.*;

ArrayList<String> imageNames = new ArrayList<String>();
ArrayList<PImage> Images = new ArrayList<PImage>();
HashMap<Integer,Integer> imageMap = new HashMap<Integer,Integer>();
HashMap<Integer,String> imgMp = new HashMap<Integer,String>();

PImage myImage;
pt ipos = new pt();
pt idim = new pt();
int mode = 1, grid=5;
int savedFrame = 0;
int animateMode = 0;

boolean fill = true, showIDs=false ;


void setup()
{
 size(800,800,P3D);  
 background(255);
 smooth();   
 strokeJoin(ROUND); 
 strokeCap(ROUND);
 setWheel();
 loadFromData();
}

void draw()
{
    background(#CC9933);   
    
    strokeWeight(3);
    
    
    if(showHelp) showHelp(); else myActions();  
    
    
    if(!animate && !showHelp)
    {
       //image(myImage, ipos.x, ipos.y);
       int high = 0;
       for(int i=0; i<Images.size(); i++)
       { 
           if(i == 0) 
           {
              image(Images.get(i),800 - Images.get(i).width - 20, 20);
              high += 20+Images.get(i).height;
           }
           else
           {
              image(Images.get(i),800 - Images.get(i).width - 20, high);
              high += Images.get(i).height + 20;
           }
       }  
          
        String aMode;
        if(animateMode == 1) 
        {
            aMode  = "ImageMode";   
        }
        else
        {
            aMode = "LineMode";   
        }
         scribeHeader("Selected Frame: "+mode+", ImageMode: "+aMode, 1 );  
         if(savedFrame != 0)
         {
             scribeHeader("Saved Frame: "+savedFrame+" ", 2 );
         }
    } 
}

