ArrayList<EDGE> FP = new ArrayList<EDGE>();
ArrayList<EDGE> FPA = new ArrayList<EDGE>();
ArrayList<ArrayList<EDGE>> F = new ArrayList<ArrayList<EDGE>>();
ArrayList<ArrayList<EDGE>> FA = new ArrayList<ArrayList<EDGE>>();
int FPn = 4, FPAn = 4;
int FPr = 4;
int FPAr = 6, num = 0;
int FPAi = 0;
float tt=0;
String lm="";
boolean newFourPoint=false, showFrames = false, animate = false, parabolicBlend = true, fourPoint = false, showLinear=false, showHelp= true;
boolean showCubic = false, showMid = false, showSpiral = false, expo = false, fit=false, showBezier=false, showCatmull=false, frames = false, add = false, showCenterPoint = false;
boolean moving = false, showKeyFrames = true;

void mousePressed()
{
    if ( add && keyPressed && key == 'a' && points.size()%2 == 0)
    {
        addToPoints();
    }
}

void mouseReleased()
{
    if (add && (points.size()%2 == 1) && keyPressed && key == 'a')
        addToPoints();
}

void keyPressed()
{
    if (key == 'c') {
        showCenterPoint = !showCenterPoint;
    }
    if ((key == '-')) twistPoint();
    if ((key=='T') && mousePressed == true) translateBy(MouseDrag());
    if (key == 't' && mousePressed == true) translateEdgeBy(MouseDrag());
    if ((key=='R') && mousePressed == true) rotateBy( angle( V(screenCenter(), Mouse()), V(screenCenter(), Pmouse()) ) );
    if ((key=='r') && mousePressed == true) rotateEdgeBy( angle( V(screenCenter(), Mouse()), V(screenCenter(), Pmouse()) ) );
    if ((key=='Z') && mousePressed == true) scaleBy(1.- d(screenCenter(), Mouse()) / d(screenCenter(), Pmouse()) );
    if ((key=='z') && mousePressed == true) scaleEdgeBy(1.- d(screenCenter(), Mouse()) / d(screenCenter(), Pmouse()) );
    if (key == '/') zeroTwists();
    if (key == 'u') unifyLengths();
    if (key == '0') FourPointAnimationInit();
    if (key == '=') { 
        showFrames = !showFrames;
    }
    if (key == 'A') {
        animate=!animate; 
        tt=0; 
        FourPointAnimationInit(); 
        FPAi=0; 
        showFrame();
    }
    if (key == '9')  showFourPointFrames(1);
    if (key == '8') if (parabolicBlend == true) parabolicBlend = false; 
    else parabolicBlend = true;
    if (key == '7') if (fourPoint == true) fourPoint = false; 
    else fourPoint = true;
    /* if (key=='I') {fit=!fit; if(fit) lm="I"; }
     if (key=='H') {showLinear=true; showCubic=true; showMid=true; showSpiral=true; expo=true; fit=true; showBezier=true; fourPoint=true;};
     if (key=='h') {showLinear=false; showCubic=false; showMid=false; showSpiral=false; expo=false; fit=false; showBezier=false;  fourPoint=false;};
     if (key=='L') {showLinear=!showLinear; if(showLinear) lm="L";} ;  
     if (key=='m') {showMid=!showMid; if(showMid) lm="M";} ;
     if (key=='v') {showCubic=!showCubic; if(showCubic) lm="V";} ;
     if (key=='s') {showSpiral=!showSpiral; if(showSpiral) lm="S";} ;
     if (key=='e') {expo=!expo; if(expo) lm="E";}
     if (key=='b') {showBezier=!showBezier; if(showBezier)  lm="B";} ;
     if (key=='f') {fourPoint=!fourPoint; lm="f"; } ;  
     if (key=='F') {newFourPoint=!newFourPoint; lm="F"; } ;
     if (key=='p') {parabolicBlend=!parabolicBlend; if(parabolicBlend) lm="P";} ; 
     if (key=='C') {showCatmull=!showCatmull; if(showCatmull) lm="C"; } ; */
    if (key == 'x') {
        deletePoint();
    }
    if (key == 'i') {
        if (animateMode == 1) animateMode = 0; 
        else animateMode = 1; 
        loadImages();
    }
    if (key == '[') { 
        setImage();
    }
    if (key == ']') { 
        removeImage();
    }
    if (animate) if (fourPoint) {
        newFourPoint=false; 
        FourPointAnimationInit();
    } 
    else if (parabolicBlend) {
        newFourPoint=true; 
        FourPointAnimationInit();
    }
    if (key == 'S') {
        saveFiles(); 
        println("Saving Points");
    }
    if (key == 'U') {
        loadFiles(); 
        println("Loading Points");
    }
    if (key == ' ') {
        showHelp = !showHelp;
    }
    if (key == '+') {
        if (animateMode == 1 && !animate)addImages();
    }
    if (key == 'k') {
        showKeyFrames=!showKeyFrames;
    }


    // TO DO  
    if (key == '1') {
        frames = true; 
        mode = 1; 
        showFrame();
    }
    if (key == '2') {
        frames = true; 
        mode = 2; 
        showFrame();
    }
    if (key == '3') {
        frames = true; 
        mode = 3; 
        showFrame();
    }
    if (key == '4') {
        frames = true; 
        mode = 4; 
        showFrame();
    }

    if (key == '!') {
        saveFrames(1); 
        savedFrame = 1; 
        mode = 1;
    }
    if (key == '@') {
        saveFrames(2); 
        savedFrame = 2; 
        mode = 2;
    }
    if (key == '#') {
        saveFrames(3); 
        savedFrame = 3; 
        mode = 3;
    }
    if (key == '$') {
        saveFrames(4); 
        savedFrame = 4; 
        mode = 4;
    } 

    //if(key == '2') FourPointSpiralInitialize() ;
};



void myActions()
{
    background(white);
    updateN();
    stroke(black);
    showPoints();
    drawEdges();                                                            // Draws All Points
    drawEdge();                                                             // Draws the incomplete Edge

        // Set all conditions
    //if(points.size()%2 == 1) moving = true;
    if (!mousePressed)pickClosestPoint();
    if (!keyPressed && mousePressed ) dragPoint(); 
    if (!animate && !showHelp) add = true; 
    else add = false ; 
    if (add && !keyPressed)
    {   
        if (!points.isEmpty() && points.size()%2 == 1) 
        {
            points.remove(points.size()-1);
        }
    }


    //    
    if (showFrames) 
    {
        if (fourPoint)
        {
            stroke(grey);
            newFourPoint = false;
            showFourPointFrames(1);
        }   

        if (parabolicBlend)
        {
            stroke(grey);
            newFourPoint = true;
            showFourPointFrames(1);
        }

        for (float t=0; t<=1.0; t+=1.0/16/3)
        {
            showAll(t);
        }
    }  
    // Improve Efficiency
    if (showKeyFrames)
    {  
        stroke(lbrown);
        if (!eF1.isEmpty() && mode != 1)
        {
            
            for (int i=0; i<eF1.size(); i++)
            {   
                if (animateMode == 1)
                {
                    if (imageMap.containsKey(i))
                        createGrid(4, eF1.get(i), Images.get(imageMap.get(i)));
                    else 
                        eF1.get(i).show();
                }
                else
                    eF1.get(i).show();                
            }

    }
    if (!eF2.isEmpty() && mode != 2)
    {        
        for (int i=0; i<eF2.size(); i++)
        {   
            if (animateMode == 1)
                {
                    if (imageMap.containsKey(i))
                        createGrid(4, eF2.get(i), Images.get(imageMap.get(i)));
                    else 
                        eF2.get(i).show();
                }
                else
                    eF2.get(i).show();   
        }
    }
    if (!eF3.isEmpty() && mode != 3)
    {
        for (int i=0; i<eF3.size(); i++)
        {   
            if (animateMode == 1)
                {
                    if (imageMap.containsKey(i))
                        createGrid(4, eF3.get(i), Images.get(imageMap.get(i)));
                    else 
                        eF3.get(i).show();
                }
                else
                    eF3.get(i).show();   
        }
    }
    if (!eF4.isEmpty() && mode != 4)
    {
        for (int i=0; i<eF4.size(); i++)
        {   
            if (animateMode == 1)
                {
                    if (imageMap.containsKey(i))
                        createGrid(4, eF4.get(i), Images.get(imageMap.get(i)));
                    else 
                        eF4.get(i).show();
                }
                else
                    eF4.get(i).show();   
        }
    } 
    stroke(black);
}

if (animate)
{
    strokeWeight(5); 
    mode = 0;
    showFrame();
    if (fourPoint)
    {
        stroke(grey); 
        newFourPoint=false; 
        showFourPointFrame();
    }
    if (parabolicBlend)
    {
        // stroke(green);
        newFourPoint=true;
        showFourPointFrame();
    }
    showAll(tt);
}
if (showCenterPoint) screenCenter().show();
//strokeWeight(4);

}

void showAll(float t)
{
    if (showLinear)
    {
        stroke(dgreen);
        for (int i=0; i<edges.size()/4;i++)
        {
            if (t<1./3)
            {
                S(edges.get(i*4+0), t*3, edges.get(i*4+1)).show();
            }        
            else
            {
                if (t<2./3)
                    S(edges.get(i*4+1), (t-1./3)*3, edges.get(i*4+2)).show();
                else
                    S(edges.get(i*4+2), (t-2./3)*3, edges.get(i*4+3)).show();
            }
        }
    }
    if (showCubic)
    {
        stroke(cyan);
        for (int i=0; i< edges.size()/4; i++)
        {   
            cubic(edges.get(i*4+0), edges.get(i*4+1), edges.get(i*4+2), edges.get(i*4+3), t).show();
        }
    }
    if (showMid)
    {
        stroke(blue);
        for (int i=0; i<edges.size()/4;i++)
        {
            retrofitMSR(edges.get(i*4+0), edges.get(i*4+1), edges.get(i*4+2), edges.get(i*4+3), t).show();
        }
    }
    if (showSpiral)
    {
        expoTemp=false;
        stroke(red);
        for (int i=0; i<edges.size()/4;i++)
        {
            if (t<1./3)
            {
                R(edges.get(i*4+0), t*3, edges.get(i*4+1)).show();
            }
            else
            {
                if (t<2./3)   
                    R(edges.get(i*4+1), (t-1./3)*3, edges.get(i*4+2)).show();  
                else
                    R(edges.get(i*4+2), (t-2./3)*3, edges.get(i*4+3)).show();
            }
        }
    }

    if (expo)
    {
        expoTemp=true;
        stroke(dred);
        for (int i=0; i<edges.size()/4;i++)
        {
            if (t<1./3)
            {
                R(edges.get(4*i+0), t*3, edges.get(4*i+1)).show();
            }
            else
            {
                if (t<2./3)   
                    R(edges.get(4*i+1), (t-1./3)*3, edges.get(4*i+2)).show();  
                else
                    R(edges.get(4*i+2), (t-2./3)*3, edges.get(4*i+3)).show();
            }
        }    
        expoTemp = false;
    }

    if (showBezier)
    {
        stroke(dyellow);    
        for (int i=0; i<edges.size()/4; i++)
        { 
            bezier(edges.get(4*i+0), edges.get(4*i+1), edges.get(4*i+2), edges.get(4*i+3), t).show();
        }
    }

    if (fit)
    {
        stroke(magenta);
        expoTemp = false;
        for (int i=0; i<edges.size()/4;i++)
        {
            retrofitSpiral(edges.get(4*i+0), edges.get(4*i+1), edges.get(4*i+2), edges.get(4*i+3), t).show();
        }
    }

    if (showCatmull) 
    {
        stroke(orange); 

        for (int i=0; i<edges.size()/4;i++)
        {
            if (t<1./3) 
                catmull(edges.get(4*i+0), edges.get(4*i+0), edges.get(4*i+1), edges.get(4*i+2), t*3).show(); 
            else 
            {
                if (t<2./3) 
                    catmull(edges.get(4*i+0), edges.get(4*i+1), edges.get(4*i+2), edges.get(4*i+3), (t-1./3)*3).show(); 
                else  
                    catmull(edges.get(4*i+1), edges.get(4*i+2), edges.get(4*i+3), edges.get(4*i+3), (t-2./3)*3).show();
            } 
            if (showConstruction) 
                showCatmullConstruction(edges.get(4*i+0), edges.get(4*i+1), edges.get(4*i+2), edges.get(4*i+3));
        }
    }
}    


void showCatmullConstruction(EDGE A, EDGE B, EDGE C, EDGE D) 
{
    stroke(magenta); 
    R(A, 1./6, C, B).show(); 
    R(D, 1./6, B, C).show(); 
    stroke(blue);
};



void showFourPointFrame() 
{
    for (int i=0; i< F.size() ;i++)
    {   
        if (animateMode == 1)
        {
            if (imageMap.containsKey(i))
                createGrid(4, F.get(i).get(FPAi), Images.get(imageMap.get(i)));
            else 
                F.get(i).get(FPAi).show();
        }
        else
        {
            F.get(i).get(FPAi).show();
        }
        FPn = F.get(i).size();
    }
    FPAi++;
    if (FPAi >= FPn)
    {
        FPAi = 0;
    }
}



void FourPointAnimationInit() 
{
    FourPointSpiralInitialize();

    for (int i=0; i<FPAr; i++) 
        FourPointSpiralSubdivde();

    for (int i=0; i<F.size(); i++) 
    { 
        ArrayList<EDGE> e = F.get(i);
        ArrayList<EDGE> a = new ArrayList<EDGE>(); 
        for (int j =0; j<e.size();j++)
        {
            a.add(E(e.get(j)));
        }
        FA.add(a);
        FPAn = a.size();
    }
    FPAi=0;
}

void FourPointSpiralInit()
{
    updateStruct();
    FP.clear();
    for (int i=0; i< edges.size(); i++)
    {
        FP.add(edges.get(i));
    }
    FPn = FP.size();
}

void FourPointSpiralInitialize()
{
    updateStruct();
    F.clear();
    int j=0;
    for (int k=0; k< edges.size()/4 ; k++)
    {
        ArrayList<EDGE> e = new ArrayList<EDGE>();
        for (int i=0; i< 4; i++)
        {
            e.add(edges.get(j));  
            j++;
        }
        F.add(e);
        FPn = e.size();
    }
}

EDGE FPf(ArrayList<EDGE> fp, int i) 
{

    if (i==-1) 
        return R(fp.get(1), 2, fp.get(0)); 
    if (i==FPn) 
        return R(fp.get(FPn-2), 2, fp.get(FPn-1));
    return fp.get(i);
} 




void FourPointSpiralShowFrames()  
{  
    for (int i=0; i<F.size(); i++)
    {
        ArrayList<EDGE> e = F.get(i);
        for (int j=0; j< e.size(); j++)
        {
            e.get(j).show();
        }
    }
}

void FourPointSpiralSubdivde() 
{
    if (newFourPoint) 
        FourPointSpiralSubdivdeNew(); 
    else 
        FourPointSpiralSubdivdeOld();
}

void FourPointSpiralSubdivdeNew() 
{
    if (FPn != 0)
    {
        for (int l=0; l< F.size(); l++)  
        {
            FPn = F.get(l).size();
            EDGE[] Q = new EDGE[2*FPn-1]; 
            EDGE[] L = new EDGE[FPn]; 
            EDGE[] R = new EDGE[FPn]; 
            for (int i=1; i<FPn-1; i++) 
            { 
                L[i]=left(F.get(l).get(i-1), F.get(l).get(i), F.get(l).get(i+1)); 
                R[i]=left(F.get(l).get(i+1), F.get(l).get(i), F.get(l).get(i-1));
            }

            for (int i=0; i<FPn; i++)   
                Q[2*i]=E(F.get(l).get(i));
            for (int i=1; i<FPn-2; i++) 
                Q[2*i+1]=R(R[i], 0.5, L[i+1]);

            Q[1]=E(L[1]); 
            Q[2*FPn-3]=E(R[FPn-2]);
            FPn=2*FPn-1;
            ArrayList<EDGE> e = new ArrayList<EDGE>();
            for (int i=0; i<FPn; i++) 
                e.add(E(Q[i]));         
            F.set(l, e);
        }
    }
}

void FourPointSpiralSubdivdeOld() 
{    

    if (FPn != 0)
    {

        for (int l=0; l<F.size(); l++)
        {
            FPn = F.get(l).size();
            EDGE[] L = new EDGE[2*FPn-1];
            for (int i=0; i<FPn; i++) 
            {
                L[2*i]=E(F.get(l).get(i));
                if (i<FPn-1)  
                    L[2*i+1]=FPmid(FPf(F.get(l), i-1), FPf(F.get(l), i), FPf(F.get(l), i+1), FPf(F.get(l), i+2));
            }
            FPn=2*FPn-1;
            ArrayList<EDGE> e = new ArrayList<EDGE>();
            for (int i=0; i<FPn; i++)
            { 
                e.add(E(L[i]));
            }

            F.set(l, e);
        }
    }
}

void showFourPointFrames(int r) 
{
    FourPointSpiralInitialize();
    for (int i=0; i<FPr; i++) 
        FourPointSpiralSubdivde();
    FourPointSpiralShowFrames();
}


























