/*
    SOME IMPORTANT NOTES
    
    1) No. of edges in the frames should be same.
    2) Images attached to any edge in 1 frame automatically gets attached to the other frames
    
    
    
    BUGS:
    1) Bug adding images mid way
    
    
    FEEDBACK:
    1) Is it more intutive to maintain some other property of the curve. What is being maintained here ?
    2) Blend image to differentiate
    3) Improvement in texture mapping
    4) Undo Feature

*/
