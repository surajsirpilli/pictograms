
pt Pnt(int i) {return ImagePoints.get(i);}
pt Pnt(int i, int j, int n) {int k = i*n+j; if(k<ImagePoints.size()) return ImagePoints.get(k); else return ImagePoints.get(ImagePoints.size()-1);}
int numberOfPoints = 0;


void createGrid(int w, EDGE E)
{  
    
    ImageEdge.clear();
    ImagePt.clear();
    ImagePoints.clear();
    pt p1 = E.A;
    pt p2 = E.B;
    
    
    float ratio = idim.y/idim.x;
    float t1 = E.a;
    float t2 = E.b;
    
    float d = p1.disTo(p2);
    vec dir = new vec(p1,p2);
    dir.normalize();
    vec dirN = dir.left();
    dirN.normalize();
    dirN.mul(-1);
    
    float xLength = d/(ratio*(w-1));
    float yLength = d/(w-1);       

    for(int j=0; j<w; j++)
    {
       pt imgPt1 = P(p1);
       imgPt1.translateBy(j*xLength,dirN);
       ImagePt.add(imgPt1); 
       pt imgPt2 = P(p2);
       imgPt2.translateBy(j*xLength,dirN);
       ImagePt.add(imgPt2);
    }
    for(int i=0; i<2*w; i = i+2)
    {
      ImageEdge.add(E(ImagePt.get(i),ImagePt.get(i+1),t1,t2));
    }
    for(int i=0; i<w; i++)
    {
         drawPoints(ImageEdge.get(i));
    }
    drawImage(w);
}


void createGrid(int w, EDGE E, PImage myImg)
{  
    
    ImageEdge.clear();
    ImagePt.clear();
    ImagePoints.clear();
    pt p1 = E.A;
    pt p2 = E.B;
    
    
    float ratio = myImg.height/myImg.width;
    float t1 = E.a;
    float t2 = E.b;
    
    float d = p1.disTo(p2);
    vec dir = new vec(p1,p2);
    dir.normalize();
    vec dirN = dir.left();
    dirN.normalize();
    dirN.mul(-1);
    
    float xLength = d/(ratio*(w-1));
    float yLength = d/(w-1);       

    for(int j=0; j<w; j++)
    {
       pt imgPt1 = P(p1);
       imgPt1.translateBy(j*xLength,dirN);
       ImagePt.add(imgPt1); 
       pt imgPt2 = P(p2);
       imgPt2.translateBy(j*xLength,dirN);
       ImagePt.add(imgPt2);
    }
    for(int i=0; i<2*w; i = i+2)
    {
      ImageEdge.add(E(ImagePt.get(i),ImagePt.get(i+1),t1,t2));
    }
    for(int i=0; i<w; i++)
    {
         drawPoints(ImageEdge.get(i));
    }
    drawImage(w,myImg);
}


void displayGrid(int n )
{
    for(int i=0; i<n-1; i++) 
      for(int j=0; j<n-1; j++) {
         edge(Pnt(i,j,n),Pnt(i+1,j,n));
         edge(Pnt(i,j,n),Pnt(i,j+1,n));
         }
    for(int i=0; i<n-1; i++) edge(Pnt(i,n-1,n),Pnt(i+1,n-1,n));
    for(int j=0; j<n-1; j++) edge(Pnt(n-1,j,n),Pnt(n-1,j+1,n)); 
}

void drawImage(int n)
{
    int k = numberOfPoints;
    strokeWeight(0);
    noFill();
      float in = 1./(n-1);
      float jn = 1./20;
    textureMode(NORMAL);       // texture parameters in [0,1]x[0,1]
    beginShape(QUADS); 
    for (int i=0; i<n-1; i++) 
    {
      beginShape(QUAD_STRIP); 
      texture(myImage); 
      for (int j=0; j<k; j++) 
      { 
        v(Pnt(i,j,k), in*i,jn*j); 
        v(Pnt(i+1,j,k),in*(i+1), jn*j); 
      }
      endShape();
    }
    strokeWeight(5);
}


void drawImage(int n, PImage myImg)
{
    int k = numberOfPoints;
    strokeWeight(0);
    noFill();
      float in = 1./(n-1);
      float jn = 1./20;
    textureMode(NORMAL);       // texture parameters in [0,1]x[0,1]
    beginShape(QUADS); 
    for (int i=0; i<n-1; i++) 
    {
      beginShape(QUAD_STRIP); 
      texture(myImg); 
      for (int j=0; j<k; j++) 
      { 
        v(Pnt(i,j,k), in*i,jn*j); 
        v(Pnt(i+1,j,k),in*(i+1), jn*j); 
      }
      endShape();
    }
    strokeWeight(5);
}


void drawPoints(EDGE E)
{
      pt A = E.A;
      pt B = E.B;
      float a = E.a;
      float b = E.b;
      float s=biArcSolve(A,U(V(A,B)).makeRotatedBy(a),U(V(B,A)).makeRotatedBy(b),B);
      float d=d(A,B)*3;
      float r=d*s/(d+s); 
      numberOfPoints = 0;
      for(float i=0; i<1.05; i=i+0.05)
      {     
           float x = bezierPoint(A.x, S(A,r,U(V(A,B)).makeRotatedBy(a)).x, S(B,r,U(V(B,A)).makeRotatedBy(b)).x, B.x, i);   
           float y = bezierPoint(A.y, S(A,r,U(V(A,B)).makeRotatedBy(a)).y, S(B,r,U(V(B,A)).makeRotatedBy(b)).y, B.y, i);
           pt imgPt = new pt(x,y);
           
           ImagePoints.add(imgPt);
           numberOfPoints++;
      }   
}


void loadImages()
{
    String[] values = new String[imageNames.size()];
    for(int i=0; i<imageNames.size();i++)
    {
           values[i] = imageNames.get(i);
    }
    String numImages = (String)JOptionPane.showInputDialog("Enter the Number of Images to be Loaded");  
    if(numImages != null)
    {  
        num = Integer.parseInt(numImages);
        for(int i=0; i<num; i++)
        {
          String imageName = (String)JOptionPane.showInputDialog(null,"Choose an Image to load","Image Select",JOptionPane.PLAIN_MESSAGE,null,values,"bottle");   
          StringBuffer sb = new StringBuffer();
         
          if(imageName != null)
          {
              sb.append("data/"); sb.append(imageName);sb.append(".png");     
              PImage mImage = loadImage(sb.toString()); 
              Images.add(mImage);
              imgMp.put(i,sb.toString());
          }
        }  
    }
}

void setImage()
{
    // attach Image to the closest edge.........    
    if(animateMode != 1)
    {
     JOptionPane.showMessageDialog(null, "Please Switch to Animate Mode by pressing \"i\"", "ALERT", JOptionPane.ERROR_MESSAGE);   
    }
    if(!edges.isEmpty() && animateMode == 1)
    {
        int edge = p/2;
        int edgeImage = Integer.parseInt((String)JOptionPane.showInputDialog("Enter the Image Index (Starting from 0)"));
        if(edgeImage > Images.size()-1) edgeImage = Images.size()-1;
        imageMap.put(edge,edgeImage);
        images++;    
    }
}

void addImages()
{
     String name = (String)JOptionPane.showInputDialog("Enter the name of the image you want to add");    
     imageNames.add(name);
}

void removeImage()
{   
     if(!edges.isEmpty())
     {
         int edge = p/2;    
         imageMap.remove(edge);
         images--;
     }
}


void loadFromData()
{
    imageNames.add("bottle");
    imageNames.add("bowlingpin");
    imageNames.add("stickfig");
    imageNames.add("arrow1");
    imageNames.add("arrow2");
    imageNames.add("arrow3");
    imageNames.add("roundarrow1");
    imageNames.add("bus");
    imageNames.add("man");
    imageNames.add("plane");
    imageNames.add("cola");
    imageNames.add("opener");
}










