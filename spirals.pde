//************************************************************************
//**** SPIRALS
//************************************************************************
 
pt spiral(pt A, pt G, float s, float a) {return S(G,s,R(A,a,G));}  
//pt spiral(pt A, pt G, float s, float a, float t) {return S(G,pow(s,t),R(A,t*a,G));}  //***** logarithmic
pt spiral(pt A, pt G, float s, float a, float t) {
//  if(expoTemp) {float u=(pow(s,t)-1)/(s-1); return S(G,pow(s,u),R(A,u*a,G));}
  if(expoTemp) {return S(G,1.+t*(s-1),R(A,t*a,G));}
  else return S(G,pow(s,t),R(A,t*a,G));
   }  //***** corrected
boolean expoTemp=false; // used to define position on logaritymic spiral (equi-angle or tempered ratio)

//pt spiral(pt A, pt G, float s, float a, float t) {return S(G,1+t*(s-1),R(A,t*a,G));}  // linear

pt spiralCenter(pt A, pt B, pt C, pt D) { // Jarek second version, computes center of spiral that takes A to C and B to D
  float a = spiralAngle(A,B,C,D); 
  float z = spiralScale(A,B,C,D);
  return spiralCenter(a,z,A,C);
  }
float spiralAngle(pt A, pt B, pt C, pt D) {return angle(V(A,B),V(C,D));}
float spiralScale(pt A, pt B, pt C, pt D) {return d(C,D)/d(A,B);}

pt spiralCenter(float a, float z, pt A, pt C) {
  float c=cos(a), s=sin(a);
  float D = sq(c*z-1)+sq(s*z);
  float ex = c*z*A.x - C.x - s*z*A.y;
  float ey = c*z*A.y - C.y + s*z*A.x;
  float x=(ex*(c*z-1) + ey*s*z) / D;
  float y=(ey*(c*z-1) - ex*s*z) / D;
  //  if(showConstruction) {stroke(orange); noFill(); P(x,y).show(6);};
  return P(x,y);
  }

pt spiralCenterJarek(pt A, pt B, pt C, pt D) {  // Jarek's first version: computes center of spiral that takes A to C and B to D
  float m=d(C,D)/d(A,B); float n=d(C,D)*d(A,B);
  vec AB=V(A,B);   vec CD=V(C,D); vec AC=V(A,C);
  float c=dot(AB,CD)/n;  float s=dot(R(AB),CD)/n;
  float AB2 = dot(AB,AB);  float a=dot(AB,AC)/AB2;  float b=dot(R(AB),AC)/AB2;
  float x=(a-m*( a*c+b*s)); float y=(b-m*(-a*s+b*c));
  float d=1+m*(m-2*c);  if((c!=1)&&(m!=1)) { x/=d; y/=d; };
  if(showConstruction) {fill(cyan); S(S(A,x,AB),y,R(AB)).show(4);};
  return S(S(A,x,AB),y,R(AB));
  }

pt spiralCenterMichael(pt A, pt B, pt C, pt D) {  // derived from a formula provided by Michael Hale CS3451-S2008 
  vec AB = V(A,B); vec CD = V(C,D); vec AC = V(A,C); vec M = A(AB,-1,CD);
  float c=d(M,AC), s=d(R(M),AC), h=sqrt(sq(c)+sq(s)); c/=h; s/=h;  // to avoid atan2 call
  float t = 0; if(abs(M.x*c-M.y*s)>0.0000001) t=AC.x/( M.x*c-M.y*s); else println("degenerate spiral");
  return S(A,t,V(AB.x*c-AB.y*s,AB.x*s+AB.y*c));
  }


