ArrayList<EDGE> edges = new ArrayList<EDGE>();
ArrayList<pt> points = new ArrayList<pt>();
ArrayList<Float> twists = new ArrayList<Float>();

ArrayList<Integer> f1 = new ArrayList<Integer>();
ArrayList<Integer> f2 = new ArrayList<Integer>();
ArrayList<Integer> f3 = new ArrayList<Integer>();
ArrayList<Integer> f4 = new ArrayList<Integer>();

ArrayList<pt> F1 = new ArrayList<pt>();
ArrayList<pt> F2 = new ArrayList<pt>();
ArrayList<pt> F3 = new ArrayList<pt>();
ArrayList<pt> F4 = new ArrayList<pt>();

ArrayList<Float> tF1 = new ArrayList<Float>();
ArrayList<Float> tF2 = new ArrayList<Float>();
ArrayList<Float> tF3 = new ArrayList<Float>();
ArrayList<Float> tF4 = new ArrayList<Float>();

ArrayList<EDGE> eF1 = new ArrayList<EDGE>();
ArrayList<EDGE> eF2 = new ArrayList<EDGE>();
ArrayList<EDGE> eF3 = new ArrayList<EDGE>();
ArrayList<EDGE> eF4 = new ArrayList<EDGE>();

ArrayList<pt> ImagePt = new ArrayList<pt>();
ArrayList<pt> ImagePoints = new ArrayList<pt>();
ArrayList<EDGE> ImageEdge = new ArrayList<EDGE>();
int images = 0;
int lastSavedFrame = 0;
boolean save1=false, save2=false, save3=false, save4=false;

boolean showConstruction = false;
boolean loopIsClosed = true;
int n=points.size();                        // current number of points
int p=0; // index to the currently selected vertex being dragged

int next(int j) 
{  
    if (j==n-1) 
    {
        return (0);
    }  
    else 
    {
        return(j+1);
    }  
}  // next point in loop

int prev(int j) 
{  
    if (j==0) 
    {
        return (n-1);
    }  
    else 
    {
        return(j-1);
     }  
}  

void updateN() 
{
    n = points.size();
}

void addToPoints()
{
    
    pt closePt = Mouse();
   
    if(points.size()%2 == 1)
    {
         if(closePt.disTo(points.get(points.size()-1)) > 2)  
         {
            points.add(closePt);  
            twists.add(0.0);   
         }
    }
    else
    {
        points.add(closePt);  
        twists.add(0.0);   
    }
    
    if(points.size()%2 == 0)
    { 
        addToEdges();
    } 
}


void saveFrames(int m)
{
    if(m == 1)
    {
        F1.clear();
        tF1.clear();
        eF1.clear();
        for(int i=0; i<points.size(); i++)
        {
            F1.add(P(points.get(i)));
            tF1.add(twists.get(i));
        }
        
        for(int i=0; i<edges.size(); i++)
        {
            eF1.add(E(edges.get(i)));   
        }
        lastSavedFrame = 1;
        save1=true;
    }
    if(m == 2)
    {
        F2.clear();
        tF2.clear();
        eF2.clear();
        for(int i=0; i<points.size(); i++)
        {
            F2.add(P(points.get(i)));
            tF2.add(twists.get(i));
        }
        
        for(int i=0; i<edges.size(); i++)
        {
            eF2.add(E(edges.get(i)));   
        }
        lastSavedFrame = 2;
        save2=true;
    }
    if(m == 3)
    {
        F3.clear();
        tF3.clear();
        eF3.clear();
        for(int i=0; i<points.size(); i++)
        {
            F3.add(P(points.get(i)));
            tF3.add(twists.get(i));
        }
        
        for(int i=0; i<edges.size(); i++)
        {
            eF3.add(E(edges.get(i)));   
        }
        lastSavedFrame = 3;
        save3=true;
        
    }
    if(m == 4)
    {
        F4.clear();
        tF4.clear();
        eF4.clear();
        for(int i=0; i<points.size(); i++)
        {
            F4.add(P(points.get(i)));
            tF4.add(twists.get(i));
        }
        
        for(int i=0; i<edges.size(); i++)
        {
            eF4.add(E(edges.get(i)));   
        }
        lastSavedFrame = 4;
        save4= true;
    }   
    
    if(lastSavedFrame == 1 && !save2 && !save3 && !save4)
    {  
          F2.clear();F3.clear();F4.clear();
          eF3.clear(); eF2.clear(); eF4.clear();
          tF2.clear(); tF3.clear(); tF4.clear();  
          for(int i=0; i<F1.size(); i++)
          {
              F2.add(P(F1.get(i)));
              F3.add(P(F1.get(i)));
              F4.add(P(F1.get(i)));       
          }
          for(int i=0; i<tF1.size(); i++)
          {
              tF2.add(tF1.get(i));  
              tF3.add(tF1.get(i));
              tF4.add(tF1.get(i));
          }
          for(int i=0; i<eF1.size(); i++)
          {
              eF2.add(E(eF1.get(i)));
              eF3.add(E(eF1.get(i)));
              eF4.add(E(eF1.get(i)));
          } 
    }
    if(lastSavedFrame == 2 && !save3 && !save4)
    {
          F3.clear();F4.clear();
          eF3.clear();eF4.clear();
          tF3.clear(); tF4.clear();     
          for(int i=0; i<F2.size(); i++)
          {
              F3.add(P(F2.get(i)));
              F4.add(P(F2.get(i)));         
          }
          for(int i=0; i<tF2.size(); i++)
          {
              tF3.add(tF2.get(i));
              tF4.add(tF2.get(i));
          }
          for(int i=0; i<eF2.size(); i++)
          {
              eF3.add(E(eF2.get(i)));
              eF4.add(E(eF2.get(i)));
          }        
    }
    if(lastSavedFrame == 3 && !save4)
    {
          F4.clear();
          eF4.clear();
          tF4.clear(); 
          for(int i=0; i<F3.size(); i++)
          {
             F4.add(P(F3.get(i)));
          }
          for(int i=0; i<tF3.size(); i++)
          {
             tF4.add(tF3.get(i)); 
          }
          for(int i=0; i<eF3.size(); i++)
          {
             eF4.add(E(eF3.get(i)));         
          }    
     }
     
     equateFrames();
}

void equateFrames()
{
    int greater;
    int smaller;
    boolean equal = true;
    
    if(F1.size() < F2.size())
    {
      F2.clear();
      for(int i=0; i<F1.size(); i++) 
      {
           F2.add(P(F1.get(i)));   
      }
      eF2.clear();
      for(int i=0; i<eF1.size(); i++) 
      {
           eF2.add(E(eF1.get(i)));   
      }
    }    
    else if(F1.size() > F2.size())
    {
        F1.clear();
        for(int i=0; i<F2.size(); i++)
        {
            F1.add(P(F2.get(i)));
        } 
        eF1.clear();
        for(int i=0; i<eF2.size(); i++) 
        {
           eF1.add(E(eF2.get(i)));   
        } 
        
    }
    
    if(F1.size() < F3.size())
    {
      F3.clear();
      for(int i=0; i<F1.size(); i++) 
      {
           F3.add(P(F1.get(i)));   
      }
      eF3.clear();
      for(int i=0; i<eF1.size(); i++) 
      {
           eF3.add(E(eF1.get(i)));   
      }
    }    
    else if(F1.size() > F3.size())
    {
        F1.clear();
        for(int i=0; i<F3.size(); i++)
        {
            F1.add(P(F3.get(i)));
        }
        eF1.clear();
        for(int i=0; i<eF2.size(); i++) 
        {
           eF1.add(E(eF2.get(i)));   
        }         
    }
    
    if(F1.size() < F4.size())
    {
      F4.clear();
      for(int i=0; i<F1.size(); i++) 
      {
           F4.add(P(F1.get(i)));   
      }
      eF4.clear();
      for(int i=0; i<eF1.size(); i++) 
      {
           eF4.add(E(eF1.get(i)));   
      }
    }    
    else if(F1.size() > F4.size())
    {
        F1.clear();
        for(int i=0; i<F4.size(); i++)
        {
            F1.add(P(F4.get(i)));
        }  
        eF1.clear();
        for(int i=0; i<eF2.size(); i++) 
        {
           eF1.add(E(eF2.get(i)));   
        }        
    }      
}


void showFrame()
{
  
    points.clear();
    edges.clear();
    twists.clear();
   
    if(mode == 1)
    {
     if(!F1.isEmpty())   
     {
       for(int i=0 ; i < F1.size(); i++)
       {
           points.add(P(F1.get(i)));  
           twists.add(tF1.get(i));
       }  
     }
     if(!eF1.isEmpty())
     {
       for(int i=0; i<eF1.size(); i++) edges.add(E(eF1.get(i)));  
     } 
    }
    
    if(mode == 2)
    {
     if(!F2.isEmpty())   
     {
       for(int i=0 ; i < F2.size(); i++)
       {
           points.add(P(F2.get(i)));  
           twists.add(tF2.get(i));
       }  
     }
     if(!eF2.isEmpty())
     {
       for(int i=0; i<eF2.size(); i++) edges.add(E(eF2.get(i)));  
     } 
    }
    
    if(mode == 3)
    {
     if(!F3.isEmpty())   
     {
       for(int i=0 ; i < F3.size(); i++)
       {
           points.add(P(F3.get(i)));  
           twists.add(tF3.get(i));
       }  
     }
     if(!eF3.isEmpty())
     {
       for(int i=0; i<eF3.size(); i++) edges.add(E(eF3.get(i)));  
     }          
    }
    
    if(mode == 4)
    {
     if(!F4.isEmpty())   
     {
       for(int i=0 ; i < F4.size(); i++)
       {
           points.add(P(F4.get(i)));  
           twists.add(tF4.get(i));
       }  
     }
     if(!eF4.isEmpty())
     {
       for(int i=0; i<eF4.size(); i++) edges.add(E(eF4.get(i)));  
     } 
    }
    
    if(mode == 0)
    {
        background(255);
    }
}


void updateAddToEdges()
{
    edges.clear(); 
    for(int i=0; i<eF1.size() ; i++)
    {
        edges.add(E(eF1.get(i)));
        edges.add(E(eF2.get(i)));
        edges.add(E(eF3.get(i)));
        edges.add(E(eF4.get(i)));
    }    
}



void addToEdges()
{
    edges.clear();
    for(int i=0; i<points.size()-1; i++)
    {
        edges.add( E(points.get(i), points.get(i+1),twists.get(i),twists.get(i+1)));
        i++;    
    }     
}

void clickPolygon() 
{
    pickClosestPoint();
}

void editPolygon() 
{    
    dragPoint(); 
};

void pickClosestPoint(pt M) 
{
    if(mouseIsInWindow()) 
    {
        p=0; 
        for (int i=1; i<n; i++) 
        {
            if (M.disTo(points.get(i))<M.disTo(points.get(p))) p=i; 
        }
    }
}

void pickClosestPoint() 
{
    pt M = mouse(); 
    p=0; 
    for (int i=1; i<n; i++)
    { 
        if (M.disTo(points.get(i))< M.disTo(points.get(p))) 
        {
            p=i;
        }
     } 
}

void twistPoint()
{
    if(!twists.isEmpty())
    {
        twists.set(p, twists.get(p)+ a(Pmouse(),points.get(p),Mouse()));
        drawEdge(p,'t');
    }
}

void dragPoint() 
{    
    if(!points.isEmpty())
    {   
        points.get(p).moveWithMouse();
        points.get(p).clipToWindow(); 
    }
}      // moves selected point (index p) by amount mouse moved recently

void showCenter()
{
  pt center = ScreenCenter();
  center.show();  
}

void dragEdge()
{
    if(edges.get(p/2).A.x == points.get(p).x && edges.get(p/2).A.y == points.get(p).y)
    {
       edges.get(p/2).A.moveWithMouse();
    }
    else if (edges.get(p/2).B.x == points.get(p).x && edges.get(p/2).B.y == points.get(p).y)
    {
       edges.get(p/2).B.moveWithMouse();
    }
    
}

void drawEdge(int i, char t)
{
    noFill();
    if(i%2 == 0)
    {
     edges.get(p/2).a =  twists.get(i);   
    }
    else
    {
     edges.get(p/2).b = twists.get(i);  
    }
}

void drawEdge()
{
    
    strokeWeight(4);
    if( points.isEmpty()==false && points.size()%2 != 0 )
    {
        line(points.get(points.size()-1).x,points.get(points.size()-1).y,mouseX,mouseY);
    }
}

void dragPoint(vec V) 
{
    points.get(p).translateBy(V);
    //updateEdge();
}

void deletePoint() 
{  
    if(!points.isEmpty())
    {
        if(p%2 == 0)
        {
            points.remove(p+1);
            points.remove(p);
            twists.remove(p+1);
            twists.remove(p);  
        }
        else
        {
            points.remove(p);
            points.remove(p-1);
            twists.remove(p);
            twists.remove(p-1);
        }
        edges.remove(p/2);
    }
}

void translatePoints(vec V) 
{
    for (int i=0; i<n; i++) 
    {
        points.get(i).translateBy(V); 
    }
    updateEdges();
}   



void scalePoints(float s) 
{
    for (int i=0; i<n; i++)
    {
        points.get(i).translateTowards(s,screenCenter());
    }
    updateEdges();
}  

// ************************************** MOVING *********************************

void scaleBy(float s) 
{
    for (int i=0; i<n; i++) 
    {    
        points.get(i).setTo(S(points.get(i),s,screenCenter()));
    }
    updateEdges();
}

void translateBy(vec V) 
{
    for (int i=0; i<n; i++)
    { 
        points.get(i).translateBy(V); 
    } 
    updateEdges();
}

void rotateBy(float aa) 
{
    for (int i=0; i<n; i++) 
    {  
        points.get(i).rotateBy(aa,screenCenter());
    }
    updateEdges();
} 

void translateEdgeBy(vec V)
{
     if(p%2 == 0)
     {
          points.get(p+1).translateBy(V);
          points.get(p).translateBy(V);   
     }
     else
     {
          points.get(p).translateBy(V);
          points.get(p-1).translateBy(V);         
         
     }
     updateEdges();
}

void rotateEdgeBy(float aa)
{
    if(p%2 == 0)
     {
          points.get(p+1).rotateBy(aa,screenCenter());
          points.get(p).rotateBy(aa,screenCenter());   
     }
     else
     {
          points.get(p).rotateBy(aa,screenCenter());
          points.get(p-1).rotateBy(aa,screenCenter());         
         
     }
     updateEdges();     
}

void scaleEdgeBy(float s)
{
     if(p%2 == 0)
     {
          points.get(p+1).setTo(S(points.get(p+1),s,screenCenter()));
          points.get(p).setTo(S(points.get(p),s,screenCenter()));  
     }
     else
     {
          points.get(p).setTo(S(points.get(p+1),s,screenCenter()));
          points.get(p-1).setTo(S(points.get(p),s,screenCenter()));     
         
     }
     updateEdges();
}

// ************************************** VIEWING *********************************
void showPoints() 
{
    for (int i=0; i<n; i++) 
    {
        points.get(i).show();
    }
}

void showPoints(int r) 
{
    for (int i=0; i<n; i++) 
    {    
        points.get(i).show(r);
    }
}

void drawPointIDs() 
{
    for (int i=0; i<n; i++) 
    {
        vec V=points.get(i).makeVecToAverage(points.get(prev(i)),points.get(next(i))); 
        V.normalize();
            V.scaleBy(-10); 
        V.add(-3,5); 
        points.get(i).showLabel(str(i),V);
    } 
}

void drawPolygon() 
{
    beginShape();  
    for (int i=0; i<n; i++)
    { 
        points.get(i).v(); 
    }
    endShape(CLOSE); 
}

void drawPolygon(boolean closed) 
{
    beginShape();  
    for (int i=0; i<n; i++) 
    {    
        points.get(i).v(); 
        if(closed) endShape(CLOSE); 
        else endShape();
    }
}

void drawEdge(int i) 
{
    if((0<=i)&&(i<n)) 
    points.get(i).to(points.get(next(i)));
}

void drawEdges()
{
    updateEdges();
    noFill();
    for(int i=0;i<edges.size();i++)
   {
       strokeWeight(4);
       edges.get(i).show();  
   } 
   if(animateMode == 1 && !edges.isEmpty() )
   {
       // Iterate over a hash map
           Iterator it = imageMap.keySet().iterator();
       while(it.hasNext())
       {
           Integer edgeKey = (Integer)it.next();
           Integer imageVal = imageMap.get(edgeKey);
           createGrid(4,edges.get(edgeKey),Images.get(imageVal));   
       }      
   }
}

void mouseWheel(int delta)
{
    
    if(!animate && !points.isEmpty())
    {
        pt A = points.get(p);
        
        if(A.x < mouse().x && A.y > mouse().y) delta *= -1;
        if(A.x > mouse().x && A.y > mouse().y) delta *= -1;
        
        if(!twists.isEmpty())
        {    
            if(keyPressed && key == '-')
            {
                twists.set(p,twists.get(p)+ 0.3*delta);
            }
            else if(!keyPressed)
            {
                twists.set(p,twists.get(p)+ 0.06*delta);            
            }
        }
    }
    
    //if(animate&&fourPoint) FourPointAnimationInit();
    updateEdges();
}

void updateEdges()
{
   for(int i=0; i<points.size(); i++)
   {
       if(i < 2*edges.size())
       {
           if(i%2 == 0 )
           {
               edges.get(i/2).A.x = points.get(i).x;
               edges.get(i/2).A.y = points.get(i).y;
           }
           else
           {
               edges.get(i/2).B.x = points.get(i).x;
               edges.get(i/2).B.y = points.get(i).y;
           }
       }
   }   
   
   for(int i=0; i<twists.size(); i++)
   {
     if(i < 2*edges.size())
     {  
         if(i%2 == 0 )
         {
             edges.get(i/2).a = twists.get(i); 
         }
         else
         {
              edges.get(i/2).b = twists.get(i);
         }
     }     
   }
   
  /* edges.clear();
   for(int i=0; i<points.size(); i=i+2)
   {
       edges.add(E(points.get(i),points.get(i+1),twists.get(i),twists.get(i+1)));    
   } */
   
}

void updateStruct()
{
  updateAddToEdges();   
}
// ************************************** ARCHIVAL *********************************
void saveFiles()
{
 String s = (String)JOptionPane.showInputDialog("Please Enter the filename (as 'filename'.pts)");   
 if(s != null)
 savePts(s);      
}

void savePts() {savePts("P.pts");}

void savePts(String fn) 
{ 
    int n = F1.size() + F2.size() + F3.size()+ F4.size() + 2*images;
    String [] inppts = new String [n+9];
    int s=0; 
    inppts[s++]=str(n+9)+","+str(F1.size()); 
    inppts[s++]=" ";
    for (int i=0; i<F1.size(); i++) 
    {
        inppts[s++]=str(F1.get(i).x)+","+str(F1.get(i).y)+","+str(tF1.get(i));
    }
    inppts[s++] = " ";
    for (int i=0; i<F2.size(); i++) 
    {
        inppts[s++]=str(F2.get(i).x)+","+str(F2.get(i).y)+","+str(tF2.get(i));
    }
    inppts[s++] = " ";   
    for (int i=0; i<F3.size(); i++) 
    {
        inppts[s++]=str(F3.get(i).x)+","+str(F3.get(i).y)+","+str(tF3.get(i));
    }
    inppts[s++] = " ";
    for (int i=0; i<F4.size(); i++) 
    {
        inppts[s++]=str(F4.get(i).x)+","+str(F4.get(i).y)+","+str(tF4.get(i));
    }
    inppts[s++] = " ";
    inppts[s++] = str(images);
    inppts[s++] = " ";
    Iterator it = imageMap.keySet().iterator();
    while(it.hasNext())
    {
        Integer edgeKey = (Integer)it.next();
        Integer imageVal = imageMap.get(edgeKey);
        inppts[s++] = str(edgeKey)+","+str(imageVal);   
    } 
    inppts[s++] = " ";
    Iterator im = imgMp.keySet().iterator();
    while(im.hasNext())
    {
        Integer imKey = (Integer)im.next();
        String  imgVal = imgMp.get(imKey);
        inppts[s++] = str(imKey)+","+imgVal;   
    }
    
    
    saveStrings(fn,inppts);  
}

void loadPts() {loadPts("P.pts");}

void loadFiles()
{
 String s = (String)JOptionPane.showInputDialog("Please Enter the filename (as 'filename'.pts)");   
 loadPts(s);      
}

void loadPts(String fn) { 
    F1.clear(); F2.clear(); F3.clear(); F4.clear();
    tF1.clear(); tF2.clear(); tF3.clear(); tF4.clear();
    imageMap.clear();
    imgMp.clear();
    edges.clear();
    points.clear();
    twists.clear();
    String [] ss = loadStrings(fn);
    String subpts;
    int s=0; int comma1,comma2; 
    comma1 = ss[s].indexOf(','); 
    int n = int(ss[s].substring(0,comma1));
    int k = int(ss[s].substring(comma1+1,ss[s].length()));
    s++;
    s++;
    for(int i=0; i<k; i++) 
    { 
        comma1=ss[s].indexOf(',');
        comma2=ss[s].indexOf(',',comma1+1);
        F1.add(new pt (float(ss[s].substring(0,comma1)), float(ss[s].substring(comma1+1,comma2))));
        tF1.add(float(ss[s].substring(comma2+1,ss[s].length())));
        s++;
    }
    s++;
    
    for(int i=0;i<k; i++) 
    { 
        comma1=ss[s].indexOf(',');
        comma2=ss[s].indexOf(',',comma1+1);
        F2.add(new pt (float(ss[s].substring(0,comma1)), float(ss[s].substring(comma1+1,comma2))));
        tF2.add(float(ss[s].substring(comma2+1,ss[s].length())));
        s++; 
    };
    
    s++;
    for(int i=0;i<k; i++) 
    { 
        comma1=ss[s].indexOf(',');
        comma2=ss[s].indexOf(',',comma1+1);
        F3.add(new pt (float(ss[s].substring(0,comma1)), float(ss[s].substring(comma1+1,comma2))));
        tF3.add(float(ss[s].substring(comma2+1,ss[s].length())));
        s++; 
    };
    s++;
    for(int i=0;i<k; i++) 
    { 
        comma1=ss[s].indexOf(',');
        comma2=ss[s].indexOf(',',comma1+1);
        F4.add(new pt (float(ss[s].substring(0,comma1)), float(ss[s].substring(comma1+1,comma2))));
        tF4.add(float(ss[s].substring(comma2+1,ss[s].length())));
        s++; 
    };
    s++;
    images = int(ss[s]);
    s++;
    s++;
    
    for(int i=0; i<images;i++)
    {
        comma1=ss[s].indexOf(',');
        imageMap.put(int(ss[s].substring(0,comma1)), int(ss[s].substring(comma1+1,ss[s].length())));
        s++;
    }
    s++;
    
    for(int i=0; i<images; i++)
    {   
        if(!ss[s].equals("null"))
        {    
            comma1=ss[s].indexOf(',');
            imgMp.put(int(ss[s].substring(0,comma1)), ss[s].substring(comma1+1,ss[s].length()));
            PImage mImg = loadImage(ss[s].substring(comma1+1,ss[s].length()));
            Images.add(mImg);
        }   
        s++;
    }
    
    
    for(int i=0; i<F1.size()-1; i=i+2)
    {
        eF1.add(E(F1.get(i),F1.get(i+1),tF1.get(i),tF1.get(i+1)));   
    }
    for(int i=0; i<F2.size()-1; i=i+2)
    {
        eF2.add(E(F2.get(i),F2.get(i+1),tF2.get(i),tF2.get(i+1)));   
    }
    for(int i=0; i<F3.size()-1; i=i+2)
    {
        eF3.add(E(F3.get(i),F3.get(i+1),tF3.get(i),tF3.get(i+1)));   
    }
    for(int i=0; i<F4.size()-1; i=i+2)
    {
        eF4.add(E(F4.get(i),F4.get(i+1),tF4.get(i),tF4.get(i+1)));   
    }
    if(images > 0 ) animateMode = 1;
    
    updateAddToEdges();
    showFrame();
    
}; 

//*************************************** ADJUSTING EDGES *************************

void zeroTwists() 
{
    for (int i=0; i<twists.size(); i++) 
    twists.set(i,0.0);
    updateEdges();
} 


void unifyLengths() 
{
       float L=0; 
       
       for (int j=0; j<edges.size(); j++) 
           L+=d(edges.get(j).A,edges.get(j).B); 
       L= L/edges.size();   
       for (int j=0; j<points.size(); j=j+2) 
       {
           vec V = U(V(points.get(j),points.get(j+1)));
           pt M = M(points.get(j),points.get(j+1));
           points.get(j).setTo(S(M,-L/2,V));
           points.get(j+1).setTo(S(M,L/2,V));
       }
}
      
