//************************************************************************
//**** EDGES
//************************************************************************
EDGE E(pt A, pt B, float a, float b) {
    return new EDGE(A, B, a, b);
}
EDGE E(pt A, pt B) {
    return new EDGE(A, B);
}
EDGE E(EDGE E) {
    return new EDGE(E.A, E.B, E.a, E.b);
}


class EDGE {
    pt A=P(); 
    pt B=P(); // endpoints
    float a=0, b=PI; // angles of tangent directions
    vec Vec() {
        return V(A, B);
    }
    float n2() {
        return dot(V(A, B), V(A, B));
    }
    float n() {
        return d(A, B);
    }
    EDGE(pt P, pt Q) {
        A.setTo(P); 
        B.setTo(Q);
    };
    EDGE(pt P, pt Q, float p, float q) {
        A.setTo(P); 
        B.setTo(Q); 
        a=p; 
        b=q;
    };
    void show() {
        drawCurve();
    }
    //  void show() {A.show(2); A.to(B);}
    void showTangents(float r) {
        arrow(A, r, U(V(A, B)).makeRotatedBy(a)); 
        arrow(B, r, U(V(B, A)).makeRotatedBy(b));
    }
    
    void drawCurve() {
        if (a==0 && b==0) {
            A.to(B); 
            return;
        }
        float s=biArcSolve(A, U(V(A, B)).makeRotatedBy(a), U(V(B, A)).makeRotatedBy(b), B);
        float d=d(A, B)*3;
        float r = d*s/(d+s);
        drawCubicBezier(A, S(A, r, U(V(A, B)).makeRotatedBy(a)), S(B, r, U(V(B, A)).makeRotatedBy(b)), B);
    }
    
    void show(pt c) {
        pt C=put(c); 
        A.show(2); 
        A.to(B); 
        B.to(C); 
        C.to(A);
    }
    
    pt grab(pt C) {
        return P(dot(V(A, C), V(A, B))/n2(), dot(V(A, C), R(V(A, B)))/n2() );
    }
    
    pt put(pt C) {
        return S(S(A, C.x, V(A, B)), C.y, R(V(A, B)));
    }
    
    pt m() {
        return M(A, B);
    }
    
    float a() {
        return angle(V(A, B));
    }
    
    void setLength(float L) {
        pt M=m(); 
        vec V=U(V(A, B)); 
        A.setTo(S(M, -L, V)); 
        B.setTo(S(M, L, V));
    }
}


float prevUT2 = 0; 
float biArcSolve(pt A, vec U, vec T, pt B) { // computes r such that d(A+rU,B+rT)=2r for fitting a biarc or a Bezier curve
    vec BA=V(B, A), UT=A(U, -1, T);
    float BA2=d(BA, BA), BAUT=d(BA, UT), UT2=d(UT, UT);
    float d=sq(BAUT)-BA2*(UT2-4);
    return (BAUT+sqrt(d))/(4.1-UT2);
};

float angle(EDGE A, EDGE B) {
    return angle(A.Vec(), B.Vec());
}
EDGE snap(EDGE E, pt Q) {
    vec V=V(E.m(), Q); 
    return E(S(E.A, V), S(E.B, V));
}
EDGE turn(EDGE E, float a) {
    pt M=E.m(); 
    return E(R(E.A, a, M), R(E.B, a, M));
}
EDGE scale(EDGE E, float s) {
    pt M=E.m(); 
    return E(S(M, s, E.A), S(M, s, E.B));
}

EDGE retrofitMSR(EDGE A, EDGE B, EDGE C, EDGE D, float t) {
    pt P = interpolatingBezier(A.m(), B.m(), C.m(), D.m(), t); //P.show(3);
    float a=interpolatingBezier(0, angle(A, B), angle(A, B)+angle(B, C), angle(A, B)+angle(B, C)+angle(C, D), t);   // #########
    float s=interpolatingBezier(A.n(), B.n(), C.n(), D.n(), t);   // #########
    EDGE R= snap(scale(turn(A, a), s/A.n()), P);
    R.a=interpolatingBezier(A.a, B.a, C.a, D.a, t);  
    R.b=interpolatingBezier(A.b, B.b, C.b, D.b, t);
    return R;
}

EDGE S(EDGE E0, float t, EDGE E1) {
    return E(S(E0.A, t, E1.A), S(E0.B, t, E1.B), s(E0.a, t, E1.a), s(E0.b, t, E1.b));
}

EDGE R(EDGE E0, float t, EDGE E1) { // return E0 moved by the t portion of the spiral(E0,E1)
    float a = a(E0.Vec(), E1.Vec());
    float s = E1.n() / E0.n();
    if (a!=0 || s!=1) { 
        pt G=spiralCenter(E0, E1); 
        return E( spiral(E0.A, G, s, a, t), spiral(E0.B, G, s, a, t), s(E0.a, t, E1.a), s(E0.b, t, E1.b));
    }
    else return S(E0, t, E1);
}

EDGE R(EDGE E0, float t, EDGE E1, EDGE E) {  // return E moved by the t portion of the spiral(E0,E1)  
    float a = a(E0.Vec(), E1.Vec());
    float s = E1.n() / E0.n();
    if (a!=0 || s!=1) { 
        pt G=spiralCenter(E0, E1); 
        return E( spiral(E.A, G, s, a, t), spiral(E.B, G, s, a, t), s(E0.a, t, E1.a, E.a), s(E0.b, t, E1.b, E.b));
    }
    else return E(S(E.A, t, V(E0.A, E1.A)), S(E.B, t, V(E0.B, E1.B)), s(E0.a, t, E1.a, E.a), s(E0.b, t, E1.b, E.b));
}

EDGE bezier(EDGE A, EDGE B, EDGE C, EDGE D, float t) {
    return  R( R( R(A, t, B), t, R(B, t, C) ), t, R( R(B, t, C), t, R(C, t, D) ) );
}

EDGE cubic(EDGE A, EDGE B, EDGE C, EDGE D, float t) {
    return  
        new EDGE(cubicBezier(A.A, retrofitBezierB(A.A, B.A, C.A, D.A), 
    retrofitBezierC(A.A, B.A, C.A, D.A), D.A, t), 
    cubicBezier(A.B, retrofitBezierB(A.B, B.B, C.B, D.B), 
    retrofitBezierC(A.B, B.B, C.B, D.B), D.B, t), 
    interpolatingBezier(A.a, B.a, C.a, D.a, t), 
    interpolatingBezier(A.b, B.b, C.b, D.b, t));
}

EDGE catmull(EDGE A, EDGE B, EDGE C, EDGE D, float t) {
    // if(showConstruction) {stroke(magenta); R(A,1./6,C,B).show(); R(D,1./6,B,C).show(); stroke(blue);};
    return bezier(B, R(A, 1./6, C, B), R(D, 1./6, B, C), C, t);
}

boolean edgesIntersect(pt A, pt B, pt C, pt D) {
    boolean hit=true; 
    if (isLeftTurn(A, B, C)==isLeftTurn(A, B, D)) hit=false; 
    if (isLeftTurn(C, D, A)==isLeftTurn(C, D, B)) hit=false; 
    return hit;
}
boolean edgesIntersect(pt A, pt B, pt C, pt D, float e) {
    return ((A.isLeftOf(C, D, e) && B.isLeftOf(D, C, e))||(B.isLeftOf(C, D, e) && A.isLeftOf(D, C, e)))&&
        ((C.isLeftOf(A, B, e) && D.isLeftOf(B, A, e))||(D.isLeftOf(A, B, e) && C.isLeftOf(B, A, e))) ;
}
pt linesIntersection(pt A, pt B, pt C, pt D) {
    vec AB = A.makeVecTo(B);  
    vec CD = C.makeVecTo(D);  
    vec N=CD.makeTurnedLeft();  
    vec AC = A.makeVecTo(C);
    float s = dot(AC, N)/dot(AB, N); 
    return A.makeTranslatedBy(s, AB);
}

EDGE retrofitSpiral(EDGE A, EDGE B, EDGE C, EDGE D, float t) {
    return bezier(A, retrofitBezier(A, B, C, D), retrofitBezier(D, C, B, A), D, t);
}

EDGE retrofitBezier(EDGE A, EDGE B, EDGE C, EDGE D) {
    return R(R(D, 1+2./7, C), 1+7./6, R(A, 1+5./13, B));
}
EDGE retrofitBezier2(EDGE A, EDGE B, EDGE C, EDGE D) {
    return R(A, 1+5./13, B);
}
EDGE retrofitBezier1(EDGE A, EDGE B, EDGE C, EDGE D) {
    return R(D, 1+2./7, C);
}

EDGE fourPointSpiral(EDGE A, EDGE B, EDGE C, EDGE D, float t) {
    return S(S(A, 9./8, B), 0.5, S(D, 9./8, C));
}
pt spiralCenter(EDGE A, EDGE B) {
    return spiralCenter(A.A, A.B, B.A, B.B);
}  

EDGE left(EDGE A, EDGE B, EDGE C) {
    return R(C, .125, B, R(A, .125, B, R(A, 0.5, B)));
} 
EDGE FPmid(EDGE A, EDGE B, EDGE C, EDGE D) {
    return R(R(A, 1.125, B), 0.5, R(D, 1.125, C));
}

